<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'name', 'status',
    ];
    public function user(){
        return $this-> belongsTo('App\User');
    }

}
