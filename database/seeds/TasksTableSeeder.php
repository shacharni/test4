<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert([
            [
        'name'=>'task 1',
        'user_id'=>'1',
    
    ],
    [
        'name'=>'task 2',
        'user_id'=>'2',
    
    ],
    [
        'name'=>'task 3',
        'user_id'=>'3',
    
    ],
]);
    }
}
