@extends('layouts.app')

@section('content')
<h1>Update a task</h1>
<form method='post' action="{{action('TaskController@update',$task->id)}}">
    @csrf
    @method('PATCH')

    <div class="form-group">
        <label for ="name"> task to update</label>
        <input type="text" class= "form-control" name="name" value="{{$task->name}}">
    </div>
    
    <div class = "form-group">
        <input type="submit" class="form-controll" name="submit" value="update">
    </div>
</form>
@endsection