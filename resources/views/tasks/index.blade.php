@extends('layouts.app')

@section('content')

<!DOCTYPE html>
<html>
@if (Request::is('tasks')) 
<h2><a href="{{action('TaskController@mytasks')}}">My Tasks</a></h2>
@else
<h2><a href="{{action('TaskController@index')}}">All Tasks</a></h2>
@endif
<h1> this is a task list</h1>
    <body>
    <table>
  <tr>
    <th>name</th>
  </tr>
   @foreach($tasks as $task)
  <tr>
  @can('admin') <td>@if ($task->status)
           <button  class="btn btn-link" id ="" value="1"> Done!</button>
       @else
           <button class="btn btn-link" id ="{{$task->id}}" value="0"> Mark as done</button>
       @endif </td>@endcan
  <td>{{$task->name}}</td>
  <td><a href="{{route('tasks.edit',$task->id)}}" class="btn btn-link">edit</a></td>
 @can('admin') <td> <br><form method='post' action="{{action('TaskController@destroy',$task->id)}}">
    @csrf
    @method('DELETE')

    <div class = "form-group">
        <input class="btn btn-link" type="submit" class="form-controll" name="submit" value="delete">
    </div>
</form>
</td>@endcan
  </tr>

     @endforeach
     </table>
     <a href="{{route('tasks.create')}}">Create a new task</a>
   </body>
</html>

<script>
$(document).ready(function(){
           $("button").click(function(event){
            console.log(event.target.id)
               $.ajax({
                   url:  "{{url('tasks')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type:  'put',
                   contentType: 'application/json',
                   data: JSON.stringify({'status':(event.target.value-1)*-1, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                      console.log( errorThrown );
                   }
               });               
              location.reload();
           });
       });
</script>

@endsection
